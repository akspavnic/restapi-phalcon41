<?php

use Codeception\Util\HttpCode;

class SearchUsersCest
{
    private const FIRST_NAME = 'Аркадий';
    private const LAST_NAME = 'Иванов';

    private int $userId;

    private array $userJson;

    public function _before(ApiTester $I)
    {
    }

    /**
     * @param int $userId
     */
    public function _setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @param $userJson $userJson
     */
    public function _setUserJson(array $userJson): void
    {
        $this->userJson = $userJson;
    }


    // tests
    public function createUserTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/users', [
            'firstName' => static::FIRST_NAME,
            'lastName' => static::LAST_NAME,
            'patronymic' => 'Отчество',
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $userJson = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        $userId = $I->grabDataFromResponseByJsonPath('$.id')[0];
        $firstName = $I->grabDataFromResponseByJsonPath('$.firstName')[0];
        $lastName = $I->grabDataFromResponseByJsonPath('$.lastName')[0];

        $I->assertEquals(static::FIRST_NAME, $firstName);
        $I->assertEquals(static::LAST_NAME, $lastName);
        $I->assertIsNumeric($userId);

        $this->_setUserId($userId);
        $this->_setUserJson($userJson);
    }

    public function searchCreatedUserTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/users/search?q='.static::LAST_NAME);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $I->comment($body);

        $array = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        $I->assertContains($this->userJson, $array);

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/users/'.$this->userId);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }
}
