<?php

use Codeception\Util\HttpCode;

class MethodNotAllowedCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function notFoundMethodTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/'.time());

        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $I->comment($body);

        $message = $I->grabDataFromResponseByJsonPath('$.message')[0];

        $I->assertEquals('Method not allowed', $message);
    }
}
