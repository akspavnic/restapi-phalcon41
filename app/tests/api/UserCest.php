<?php

use Codeception\Util\HttpCode;

class UserCest
{
    private int $userId;

    private string $firstName;

    /**
     * @param int $userId
     *
     * @return UserCest
     */
    public function _setUserId(int $userId): UserCest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param string $firstName
     *
     * @return UserCest
     */
    public function _setFirstName(string $firstName): UserCest
    {
        $this->firstName = $firstName;
        return $this;
    }


    public function _before(ApiTester $I)
    {
    }

    public function createTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/users', [
            'firstName' => 'test',
            'lastName' => 'test',
            'patronymic' => 'patronymic'
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $I->comment($body);

        $userId = $I->grabDataFromResponseByJsonPath('$.id')[0];
        $firstName = $I->grabDataFromResponseByJsonPath('$.firstName')[0];

        $I->assertEquals('test', $firstName);
        $I->assertIsNumeric($userId);

        $this->_setUserId((int)$userId);
        $this->_setFirstName((string)$firstName);
    }

    public function updateTest(ApiTester $I)
    {
        $newFirstName = 'test'.time();

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/users/'.$this->userId, [
            'firstName' => $newFirstName,
            'lastName' => 'test'.time(),
            'patronymic' => 'patronymic'.time()
        ]);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $I->comment($body);

        $userId = $I->grabDataFromResponseByJsonPath('$.id')[0];
        $firstName = $I->grabDataFromResponseByJsonPath('$.firstName')[0];

        $I->assertEquals($newFirstName, $firstName);
        $I->assertEquals($this->userId, $userId);

        $this->_setUserId($userId);
        $this->_setFirstName($firstName);
    }

    public function getUserTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/users/'.$this->userId);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $I->comment($body);

        $userId = $I->grabDataFromResponseByJsonPath('$.id')[0];
        $firstName = $I->grabDataFromResponseByJsonPath('$.firstName')[0];

        $I->assertEquals($this->userId, $userId);
        $I->assertEquals($this->firstName, $firstName);
    }

    public function deleteTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/users/'.$this->userId);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $body = $I->grabResponse();
        $I->comment($body);

        $status = $I->grabDataFromResponseByJsonPath('$.status')[0];
        $I->assertEquals('OK', $status);

        $I->sendDelete('/users/'.$this->userId);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $status = $I->grabDataFromResponseByJsonPath('$.status')[0];
        $message = $I->grabDataFromResponseByJsonPath('$.message')[0];

        $I->assertEquals('ERROR', $status);
        $I->assertEquals('User not found', $message);
    }
}
