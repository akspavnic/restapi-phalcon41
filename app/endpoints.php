<?php
declare(strict_types=1);

use App\Controller\V1\UsersController;
use Phalcon\Mvc\Micro\Collection;

/**
 * @var Phalcon\Mvc\Micro $application
 */

$endpoints = new Collection();
$endpoints->setHandler(UsersController::class, true);
$endpoints->setPrefix('/api/v1/users');
$endpoints->get('/', 'getUsers');
$endpoints->get('/{userId:[0-9]+}', 'getUser');
$endpoints->post('/', 'postUser');
$endpoints->put('/{userId:[0-9]+}', 'putUser');
$endpoints->delete('/{userId:[0-9]+}', 'deleteUser');
$endpoints->get('/search', 'search');
$application->mount($endpoints);