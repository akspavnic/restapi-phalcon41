<?php
declare(strict_types=1);

namespace App\Command;

use App\Service\Search\Users;
use App\Service\UserManager;
use Phalcon\Di;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class IndexerUsers extends Command
{
    protected static $defaultName = 'indexer:users';

    /**
     * @psalm-var UserManager
     */
    private UserManager $userManager;

    /**
     * IndexerUsers constructor.
     */
    public function __construct()
    {
        $this->userManager = Di::getDefault()->get(UserManager::class);

        parent::__construct();
    }


    protected function configure(): void
    {
        $this
            ->setDescription('Индексация пользователей')
            ->addOption('limit', null, InputOption::VALUE_REQUIRED, 'Количество записей на итерацию', 100)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $limit = (int)$input->getOption('limit');

        $io->comment('Индексируем пользователей');
        $bar = $io->createProgressBar();
        $bar->start();

        Users::truncate();

        $page = 1;
        while (true) {

            $users = $this->userManager->list($page, $limit);

            $batch = [];
            foreach ($users as $user) {
                $batch[] = new Users($user);
            }

            if (empty($batch)) {
                break;
            }

            Users::upsertBatch($batch, true);

            $page++;
            $bar->advance();
        }

        $bar->finish();
        $io->newLine();

        return 1;

    }
}