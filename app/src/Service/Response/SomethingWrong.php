<?php
declare(strict_types=1);

namespace App\Service\Response;

class SomethingWrong extends ApiResponse
{
    public function __construct(string $message = 'Something wrong')
    {
        parent::__construct('ERROR', $message);
    }
}