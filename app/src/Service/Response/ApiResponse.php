<?php
declare(strict_types=1);

namespace App\Service\Response;


class ApiResponse
{
    public string $status;

    public ?string $message;

    public function __construct(string $status = 'OK', ?string $message = null)
    {
        $this->status = $status;
        $this->message = $message;
    }
}