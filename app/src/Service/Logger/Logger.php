<?php
declare(strict_types=1);

namespace App\Service\Logger;

use Phalcon\Logger as PhalconLogger;
use Phalcon\Logger\Adapter\Stream;
use Phalcon\Text;

abstract class Logger
{
    protected const LOG_PATH = __DIR__ . '/../../../var/log/';

    public static function getInstance(): PhalconLogger
    {
        $classParts = explode('\\', static::class);
        end($classParts);
        $key = key($classParts);
        $loggerFileName = Text::uncamelize($classParts[$key], '-') . '.log';

        return new PhalconLogger(
            static::class,
            [
                'main' => new Stream(static::LOG_PATH. $loggerFileName),
            ]
        );
    }
}