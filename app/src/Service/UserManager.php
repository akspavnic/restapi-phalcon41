<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Users;
use Phalcon\Mvc\Model\Resultset;

class UserManager
{
    private const USERS_LIMIT = 20;

    /**
     * @param int $id
     *
     * @return Users
     * @throws \ErrorException
     */
    public function findById(int $id): Users
    {
        $user = Users::findFirstById($id);
        if (!($user instanceof Users)) {
            throw new \ErrorException('User not found');
        }

        return $user;
    }

    /**
     * @param array      $userData
     * @param Users|null $user
     *
     * @return Users
     * @throws \ErrorException
     */
    public function save(array $userData, ?Users $user = null): Users
    {
        $user ??= new Users();
        $user->assign($userData, [
            'firstName',
            'lastName',
            'patronymic'
        ]);
        $user->save();

        return $this->findById($user->getId() ?? 0);
    }

    /**
     * @param Users $user
     * @param array $userData
     *
     * @return Users
     * @throws \ErrorException
     */
    public function update(Users $user, array $userData): Users
    {
        return $this->save($userData, $user);
    }

    public function delete(Users $user): bool
    {
        return $user->delete();
    }

    /**
     * @param int $page
     *
     * @param int $limit
     *
     * @return Users[]|Resultset
     */
    public function list(int $page = 1, int $limit = null)
    {
        $limit ??= static::USERS_LIMIT;

        $params = [
            'limit' => $limit,
            'offset' => ((0===$page?1:$page) - 1) * $limit,
        ];

        return Users::find($params);
    }
}