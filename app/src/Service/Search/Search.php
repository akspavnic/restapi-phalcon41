<?php
declare(strict_types=1);


namespace App\Service\Search;


use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Db\Column;
use Phalcon\Db\Enum;
use Phalcon\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

abstract class Search
{
    protected static string $index;

    protected static string $model;

    public const SPHINX_CONNECTION = 'sphinx';

    abstract public function toParams(): array;

    public function getIndex(): string
    {
        return static::$index;
    }

    public function getModel(): string
    {
        return static::$model;
    }

    public static function getSphinxInstance(): Mysql
    {
        return Di::getDefault()->get(static::SPHINX_CONNECTION);
    }

    public static function truncate(): bool
    {
        $index = static::$index;

        return static::getSphinxInstance()->execute("TRUNCATE RTINDEX {$index}");
    }

    public static function delete(int $id): bool
    {
        $index = static::$index;
        return static::getSphinxInstance()->execute("DELETE FROM {$index} WHERE id IN ({$id})");
    }

    /**
     * @param string $query
     *
     * @return array|ResultsetInterface
     */
    public static function match(string $query)
    {
        $index = static::$index;

        $ids = array_values(static::getSphinxInstance()->fetchAll(
            "SELECT id FROM {$index} WHERE MATCH(:query)",
            Enum::FETCH_COLUMN,
            [
                'query' => $query
            ],
            [
                'query' => Column::BIND_PARAM_STR
            ]
        ));

        if (empty($ids)) {
            return [];
        }

        /**
         * @var Model $model
         */
        $model = new static::$model;
        $source = $model->getSource();
        $sqlIds = implode(',', $ids);

        $sql = "SELECT * FROM {$source} 
JOIN unnest('{{$sqlIds}}'::int[]) WITH ORDINALITY t(id, ord) USING (id)
ORDER  BY t.ord";

        return new Resultset(
            null,
            $model,
            $model->getReadConnection()->query($sql)
        );
    }

    /**
     * @param Users[] $values
     * @param bool $isInsert
     *
     * @return bool
     */
    public static function upsertBatch(array $values, bool $isInsert = false): bool
    {
        if (empty($values)) {
            return false;
        }

        $index = static::$index;
        $fields = implode(',', array_keys($values[0]->toParams()));
        $sqlValues = implode(',',
            array_map(
                /**
                 * @return string
                 */
                static function(Users $user) {
                    $resValues = [];
                    foreach (array_values($user->toParams()) as $value) {
                        if (is_string($value)) {
                            $resValues[] = static::getSphinxInstance()->escapeString($value);
                        } else {
                            $resValues[] = $value;
                        }
                    }

                    return '(' . implode(',', $resValues) . ')';
                },
                $values
            )
        );

        $sql = ($isInsert ? 'INSERT' : 'REPLACE') . " INTO {$index} ({$fields}) VALUES {$sqlValues}";

        return static::getSphinxInstance()->execute($sql);
    }

    /**
     * @param bool $isInsert
     *
     * @return bool
     */
    public function upsert(bool $isInsert = false): bool
    {
        $index = static::$index;
        $fields = implode(',', array_keys($this->toParams()));
        $placeholders = implode(',', array_fill(0, count($this->toParams()), '?'));

        $sql = ($isInsert ? 'INSERT' : 'REPLACE') . " INTO {$index} ({$fields}) VALUES ({$placeholders})";

        return static::getSphinxInstance()->execute($sql, array_values($this->toParams()));
    }
}
