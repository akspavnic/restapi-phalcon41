<?php
declare(strict_types=1);

namespace App\Service\Search;

use App\Model\Users as UsersModel;

class Users extends Search
{
    protected static string $index = 'users';

    protected static string $model = UsersModel::class;

    private int $id;

    private string $firstName;

    private string $lastName;

    private string $patronymic;

    private string $fullName;

    private int $createdAt;

    public function __construct(\App\Model\Users $user)
    {
        $this->id = (int)$user->getId();
        $this->firstName = $user->getFullName();
        $this->lastName = $user->getLastName();
        $this->patronymic = $user->getPatronymic() ?? '';
        $this->fullName = $user->getFullName();
        $this->createdAt = $user->getCreatedAtTimestamp();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function toParams(): array
    {
        return [
            'id' => $this->getId(),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'patronymic' => $this->getPatronymic(),
            'fullname' => $this->getFullName(),
            'created_at' => $this->getCreatedAt()
        ];
    }
}
