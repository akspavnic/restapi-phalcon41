<?php
declare(strict_types=1);

namespace App\Model;

use App\Service\Search\Users as SearchUser;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\ModelInterface;

/**
 * @method static findFirstById(int $id)
 */
class Users extends Model
{
    public ?int $id = null;
    protected string $firstName = '';
    protected string $lastName = '';
    protected ?string $patronymic = null;
    protected ?string $createdAt = null;

    public function initialize(): void
    {

    }

    public function setDI(DiInterface $container): void
    {
        parent::setDI($container);
    }

    public function readAttribute(string $attribute): string
    {
        return parent::readAttribute($attribute);
    }

    public function writeAttribute(string $attribute, $value): void
    {
        parent::writeAttribute($attribute, $value);
    }

    public function appendMessage(\Phalcon\Messages\MessageInterface $message): ModelInterface
    {
        return parent::appendMessage($message);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return Users
     */
    public function setFirstName(string $firstName): Users
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return Users
     */
    public function setLastName(string $lastName): Users
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @param string|null $patronymic
     *
     * @return Users
     */
    public function setPatronymic(?string $patronymic): Users
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->getLastName() . ' ' . $this->getFirstName() . ' ' . ($this->getPatronymic() ?? '');
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     *
     * @return Users
     */
    public function setCreatedAt(string $createdAt): Users
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getCreatedAtTimestamp(): int
    {
        return (new \DateTime($this->getCreatedAt() ?? 'now'))->getTimestamp();
    }

    public function columnMap(): array
    {
        return [
            'id' => 'id',
            'first_name' => 'firstName',
            'last_name' => 'lastName',
            'patronymic' => 'patronymic',
            'created_at' => 'createdAt',
        ];
    }

    public function afterCreate(): void
    {
        $this->unsertSearchIndex(true);
    }

    public function afterUpdate(): void
    {
        $this->unsertSearchIndex();
    }

    public function afterDelete(): void
    {
        if (null !== $this->getId()) {
            SearchUser::delete((int)$this->getId());
        }
    }

    private function unsertSearchIndex(bool $isInsert = false): void
    {
        $user = self::findFirstById($this->getId());
        $insertUser = new SearchUser($user);
        $insertUser->upsert($isInsert);
    }
}