<?php
declare(strict_types=1);

namespace App\Middleware;

use Phalcon\Events\Event;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;


class NotFoundMiddleware implements MiddlewareInterface
{
    /**
     * @param Event $event
     * @param Micro $application
     *
     * @return bool
     */
    public function beforeNotFound(Event $event, Micro $application): bool
    {
        $application->response->setStatusCode(405);
        $application->response->setJsonContent([
            'message' => 'Method not allowed'
        ]);
        $application->response->send();

        return false;
    }

    /**
     * @param Micro $application
     *
     * @return bool
     */
    public function call(Micro $application): bool
    {
        return true;
    }
}