<?php
declare(strict_types=1);

namespace App\Controller\V1;

use App\Model\Users;
use App\Service\Response\ApiResponse;
use App\Service\Response\SomethingWrong;
use App\Service\Response\Success;
use App\Service\Search\Users as SearchUsers;
use App\Service\UserManager;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\Resultset;

class UsersController extends Controller
{
    /**
     * @return UserManager
     */
    private function getUserManager(): UserManager
    {
        return $this->getDI()->get(UserManager::class);
    }

    public function setDI(DiInterface $container): void
    {
        parent::setDI($container);
    }

    /**
     * @return Users[]|Resultset
     */
    public function getUsers()
    {
        return $this->userManager->list((int)$this->request->getQuery('p', 'int', 1));
    }

    /**
     * @param int $userId
     *
     * @return Users|SomethingWrong
     */
    public function getUser(int $userId)
    {
        try {
            return $this->getUserManager()->findById($userId);
        } catch (\ErrorException $e) {
            return new SomethingWrong($e->getMessage());
        }
    }

    /**
     * @param int $userId
     *
     * @return Users|ApiResponse
     */
    public function putUser(int $userId)
    {
        $userData = $this->request->getJsonRawBody(true);

        //todo: validate input params

        try {
            return $this->getUserManager()->update(
                $this->getUserManager()->findById($userId),
                $userData
            );
        } catch (\ErrorException $e) {
            return new SomethingWrong($e->getMessage());
        }
    }

    /**
     * @return Users
     * @throws \ErrorException
     */
    public function postUser(): Users
    {
        $userData = $this->request->getJsonRawBody(true);

        //todo: validate input params

        return $this->getUserManager()->save($userData);
    }

    /**
     * @param int $userId
     *
     * @return ApiResponse
     */
    public function deleteUser(int $userId): ApiResponse
    {
        try {
            $this->getUserManager()->delete($this->getUserManager()->findById($userId));
        } catch (\ErrorException $e) {
            return new SomethingWrong($e->getMessage());
        }

        return new Success();
    }

    /**
     * @return Users[]|Resultset|ApiResponse
     */
    public function search()
    {
        $query = $this->request->getQuery('q', 'string', null);
        if (empty($query)) {
            return new SomethingWrong('q is require and not must be empty');
        }

        return SearchUsers::match($query);
    }
}