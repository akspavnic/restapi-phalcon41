<?php
declare(strict_types=1);


require_once __DIR__ . '/bootstrap.php';

use App\Middleware\NotFoundMiddleware;
use App\Service\Logger\Error as ErrorLogger;
use App\Service\Response\SomethingWrong;
use Phalcon\Mvc\Micro;
use Phalcon\Events\Manager as EventsManager;


$eventsManager = new EventsManager();
$eventsManager->attach('micro:beforeNotFound', new NotFoundMiddleware());

/**
 * @var Phalcon\Di\FactoryDefault $container
 */
$application = new Micro($container);
$application->setEventsManager($eventsManager);

$application->after(static function() use ($application): void {
    $application->response->setContentType('application/json', 'utf-8');
    $application->response->setCache(0);

    $isRedirect = ($application->getReturnedValue() instanceof Phalcon\Http\Response);
    if ($isRedirect && !$application->response->isSent()) {
        $application->response->send();
        return;
    }

    $returnedValue = $application->getReturnedValue();

    !$application->response->getContent() ? $application->response->setJsonContent($returnedValue) : null;
    !$application->response->isSent() && $application->response->send();
});

require_once __DIR__ . '/endpoints.php';

try{
    $application->handle($_SERVER['REQUEST_URI']);
} catch (\Exception|\Error $e) {
    /**
     * @var Phalcon\Logger $loggerError
     */
    $loggerError = $application->getDI()->get(ErrorLogger::class);
    $loggerError->critical(json_encode([
        'message' => $e->getMessage(),
        'code' => $e->getCode(),
        'trace' => $e->getTraceAsString(),
    ], JSON_THROW_ON_ERROR));

    $application->response->setStatusCode(500);
    $application->response->setJsonContent(new SomethingWrong());
    $application->response->send();
}