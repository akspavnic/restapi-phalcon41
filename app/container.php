<?php
declare(strict_types=1);

use App\Service\Logger\Debug;
use App\Service\Logger\Error;
use App\Service\UserManager;
use Phalcon\Db\Adapter\Pdo\Postgresql;
use Phalcon\Db\Adapter\Pdo\Mysql;

$container = new Phalcon\Di\FactoryDefault();

$container->setShared('db', /** @return Postgresql */ function () {
    return new Postgresql([
        'host' => $_ENV['DB_HOST'],
        'dbname' => $_ENV['DB_DATABASE'],
        'port' => $_ENV['DB_PORT'],
        'username' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
        'schema' => $_ENV['DB_SCHEMA'],
    ]);
});

$container->setShared(App\Service\Search\Search::SPHINX_CONNECTION, /** @return Mysql */ function () {
    return new Mysql([
        'host' => $_ENV['SPHINX_HOST'],
        'dbname' => $_ENV['SPHINX_INDEX'],
        'port' => $_ENV['SPHINX_PORT'],
    ]);
});

//$container->set('redis', /** @return Predis\Client */ function () {
//    return new Predis\Client([
//        'scheme' => 'tcp',
//        'host'   => $_ENV['REDIS_HOST'],
//        'port'   => $_ENV['REDIS_PORT'],
//    ]);
//}, true);

$container->setShared(Error::class, Error::getInstance());
$container->setShared(Debug::class, Debug::getInstance());
$container->set(UserManager::class, new UserManager());

return $container;