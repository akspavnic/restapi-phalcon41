<?php
declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

final class Users extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($b = 0; $b < 10; $b++) {
            $data = [];

            for ($i = 0; $i < 100; $i++) {
                $data[] = [
                    'first_name'    => $faker->firstName,
                    'last_name'     => $faker->lastName,
                    'patronymic' => $faker->realText(20, 1),
                ];
            }

            $this->table('users')->insert($data)->saveData();
        }
    }
}
