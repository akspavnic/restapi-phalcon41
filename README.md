## Build containers and run services

```
docker-compose up -d --build
```

## .env

Change value of variable PROD = 0|1. Default 0. \
Before start app service will be run scripts:

|env|automatically run scripts before start app service | you can run scripts |
|---------------|----|---- |
|PROD=0         | composer install, <br>vendor/bin/phinx migrate, <br>vendor/bin/phinx seed:run              | vendor/bin/psalm <br> vendor/bin/codecept run api|           
|PROD=1         | composer install --no-dev, <br>vendor/bin/phinx migrate              |   &mdash;        |


## Useful commands

### Install vendors

```
docker-compose exec app composer install
```

### Migrate

```
docker-compose exec app vendor/bin/phinx migrate
```

### Seed users

```
docker-compose exec app vendor/bin/phinx seed:run
```

### Indexing users

```
docker-compose exec app bin/console indexer:users -vvv
```

### Run psalm

```
docker-compose exec app vendor/bin/psalm
```

### Run tests

```
docker-compose exec app vendor/bin/codecept run api
```