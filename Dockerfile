FROM php:7.4-fpm-alpine3.11

ENV COMPOSER_ALLOW_SUPERUSER=1

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk add --update --no-cache \
    nano \
    bash \
    build-base \
    autoconf \
    coreutils \
    icu-libs \
    make \
    gcc \
    g++ \
    curl \
    php7-dev \
    pkgconfig \
    icu-libs \
    icu-dev \
    libcurl \
    curl-dev \
    libpng \
    libpng-dev \
    pcre \
    pcre-dev \
    libzip \
    libzip-dev \
    openssl \
    openssl-dev \
    rabbitmq-c \
    rabbitmq-c-dev \
    libjpeg-turbo \
    libjpeg-turbo-dev \
    jpeg-dev \
    freetype \
    freetype-dev \
    postgresql \
    postgresql-dev \
    nginx \
    && pecl install psr \
    && docker-php-ext-enable psr \
    && pecl install redis \
    && docker-php-ext-enable redis \
    && pecl install amqp \
    && docker-php-ext-enable amqp \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j "$(nproc)" opcache intl curl gd pcntl shmop zip tokenizer sockets pdo pdo_pgsql pgsql pdo_mysql \
    && pecl install phalcon \
    && docker-php-ext-enable phalcon \
    && echo "$(curl -sS https://composer.github.io/installer.sig) -" > composer-setup.php.sig \
    && curl -sS https://getcomposer.org/installer | tee composer-setup.php | sha384sum -c composer-setup.php.sig \
    && php composer-setup.php && rm composer-setup.php* \
    && chmod +x composer.phar && mv composer.phar /usr/bin/composer \
    && apk del build-base \
               autoconf \
               coreutils \
               make \
               php7-dev \
               pkgconfig \
               icu-dev \
               curl-dev \
               libpng-dev \
               pcre-dev \
               libzip-dev \
               openssl-dev \
               rabbitmq-c-dev \
               libjpeg-turbo-dev \
               jpeg-dev \
               freetype-dev \
               postgresql-dev \
               gcc \
               g++ \
    && rm -rf /var/cache/apk/*

RUN wget -O /tmp/s6-overlay-amd64.tar.gz https://github.com/just-containers/s6-overlay/releases/latest/download/s6-overlay-amd64.tar.gz \
    && tar xzf /tmp/s6-overlay-amd64.tar.gz -C / \
    && rm /tmp/s6-overlay-amd64.tar.gz

COPY ./etc/php-fpm/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY ./etc/php-fpm/app.ini /usr/local/etc/php/conf.d/phpfpm.ini

# nginx
COPY ./etc/nginx/nginx.conf /etc/nginx/
COPY ./etc/nginx/app.conf /etc/nginx/conf.d/
RUN rm /etc/nginx/conf.d/default.conf \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && ln -sf /dev/null /var/log/nginx/access.log

ENV PROD=0

# s6
COPY ./etc/cont-init.d /etc/cont-init.d
COPY ./etc/services.d /etc/services.d

WORKDIR /var/www/html

COPY app/ /var/www/html

RUN mkdir -p /var/www/html/var \
    && mkdir -p mkdir -p /var/www/html/var/log \
    && chown -R www-data:www-data /var/www/html/var

#RUN cp .env.prod .env

#RUN composer install --no-dev \
#    && bin/console secrets:decrypt-to-local --force \
#    && composer dump-env prod \
#    && mkdir -p /var/www/html/var \
#    && chown -R www-data:www-data /var/www/html/var

EXPOSE 80

ENTRYPOINT ["/init"]